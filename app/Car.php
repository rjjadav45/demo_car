<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
     protected $fillable = [
        'car_name',
        'car_color',
        'make_date',
        'fuel_type',
        'car_details',
    ]; 
}
