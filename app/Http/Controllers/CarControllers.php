<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use DataTables;

class CarControllers extends Controller
{
    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $cars = Car::latest()->get();
            return Datatables::of($cars)
                ->addIndexColumn()
                ->editColumn('status', function($cars) {
                  return ($cars->status == 1) ? "Active" : "Inactive";
                })
                ->editColumn('created_at', function($cars) {
                  return date("Y-m-d",strtotime($cars->created_at));
                })
                ->editColumn('car_pics', function($cars) {
                    $imagesArray = json_decode($cars->car_pics);
                    $images = "<div class='row'>";
                    if(!empty($imagesArray)){
                        foreach ($imagesArray as $key => $value) {
                        $images =  $images."<div style='width:120px;margin:10px;'><img src='".asset('images/'.$value)."' height='auto;' width='100px'></div>";
                        }
                    $images = $images."</div>";
                    }
                  return $images;
                })
                ->editColumn('car_icon', function($cars) {
                    $icon = "<div class='row'>";
                    if(!empty($cars->car_icon)){
                    $icon = $icon."<div style='width:120px;margin:10px;'><img src='".asset('images/'.$cars->car_icon)."' height='auto;' width='100px'></div>";
                    }
                    $icon = $icon."</div>";
                  return $icon;
                })
                ->editColumn('make_date', function($cars) {
                  return date("Y-m-d",strtotime($cars->make_date));
                })
                ->editColumn('car_color', function($cars) {
                  return substr($cars->car_color,1);
                })
                ->addColumn('action', function($row){
                    $button = '<a href="javascript:void(0)" class="edit_btn btn btn-warning btn-sm m-1" data-toggle="modal" data-target="#editCarModel" data-id="'.$row->id.'">Edit</a> <a href="javascript:void(0)" class="delete_btns btn btn-danger btn-sm m-1" data-id="'.$row->id.'">Delete</a>';
                    return $button;
                })
                ->rawColumns(['action','car_pics','car_icon'])
                ->make(true);
        }
    	return view("cars.index");
    }

    public function store(Request $request)
    {
        $car_pics = [];

        foreach ($request->file('car_pics') as $key => $value) {
            $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images'), $imageName);
            $car_pics[] = $imageName;
        }
        $car_icon ="";
        if(isset($request->car_icon)){
            $car_icon = time().'.'.$request->car_icon->extension();  
           $request->car_icon->move(public_path('images'), $car_icon);
        }

        $car = new Car;
        $car->car_name = $request->car_name;
        $car->car_color = $request->car_color;
        $car->make_date = date("Y-m-d h:i:s",strtotime($request->year."-".$request->month."-".$request->date));
        $car->fuel_type = $request->fule_type;
        $car->car_details = $request->car_details;
        $car->car_pics = json_encode($car_pics); 
        $car->car_icon = $car_icon;
        $car->latitude = $request->latitude;
        $car->longetude = $request->longitude;
        $car->status = $request->status;
        $car->save(); 

        echo json_encode(["status" => true ,"message"=>"Car add successfully."]);
    }

    public function getCarDetails(Request $request)
    {
        $id = $request->id;
        $car = Car::find($id);
        echo json_encode($car);
    }

    public function update(Request $request)
    {

        $car = Car::find($request->id);
        $car->car_name = $request->car_name;
        $car->car_color = $request->car_color;
        $car->make_date = date("Y-m-d h:i:s",strtotime($request->year."-".$request->month."-".$request->date));
        $car->fuel_type = $request->fule_type;
        $car->car_details = $request->car_details;

        $car_pics = [];
        if(isset($request->car_pics) && !empty($request->car_pics)){
            foreach ($request->file('car_pics') as $key => $value) {
            $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images'), $imageName);
            $car_pics[] = $imageName;
            }    
            $car->car_pics = json_encode($car_pics); 
        }
        
        $car_icon ="";
        if(isset($request->car_icon)){
            $car_icon = time().'.'.$request->car_icon->extension();  
           $request->car_icon->move(public_path('images'), $car_icon);
            $car->car_icon = $car_icon;
        }
        $car->latitude = $request->latitude;
        $car->longetude = $request->longitude;
        $car->status = $request->status;
        $car->save(); 

        echo json_encode(["status" => true ,"message"=>"Car Updated successfully."]);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $car = Car::where("id",$id)->delete();
        echo json_encode(["status" => true ,"message"=>"Car Deleted successfully."]);  
    }

    public function viewCars()
    {
        $cars = Car::all();
        return view("cars.detail",compact('cars'));
    }
    
}
