<?php

namespace App\Http\Middleware;

use Closure;

class CheckFileUploaded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->files->all()) {
            $allFiles = $request->files->all();
            foreach ($allFiles as $value) {
                dd($this->getFileType($value));
            }
        }else{
            return $next($request);            
        }
    }

    function getFileType($file)
    {   
        dd($file);
        //get file origan tybe 
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $orignal_type = finfo_file($finfo, $file['pathname']);
        finfo_close($finfo);

        $path_parts = pathinfo($file['originalName']);
        $response = ['filename' => $path_parts['filename'],
                     'extension' => $path_parts['extension'],
                     'type-from-extension' => $file['type'],  
                     'type-from-mime-type' => $orignal_type,
                    ];
        
        //match file type with orignal file with extantion type
        if($orignal_type === $file['mimeType']){
            $response[] = true;
            return json_encode($response);
        }
        else{
            $response[] = false;
            return json_encode($response);      
        }
    }
}
