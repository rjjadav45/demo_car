<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('cars')->group(function () {
	Route::match(["get","post"],'/', 'CarControllers@index')->name('cars.listing');
	Route::post('/store', 'CarControllers@store')->name('cars.store');
	Route::post('/update', 'CarControllers@update')->name('cars.update');
	Route::get('/delete', 'CarControllers@delete')->name('cars.delete');
	Route::get('/getCarDetails', 'CarControllers@getCarDetails')->name('getCarDetails');
	Route::get('/viewCars', 'CarControllers@viewCars')->name('viewCars');
});

