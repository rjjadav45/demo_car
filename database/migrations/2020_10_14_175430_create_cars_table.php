<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('car_name')->nullable();
            $table->string('car_color')->nullable();
            $table->string('make_date')->nullable();
            $table->string('fuel_type')->nullable();
            $table->text('car_details')->nullable();
            $table->text('car_pics')->nullable();
            $table->string('car_icon')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longetude')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0 => Inactive, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
