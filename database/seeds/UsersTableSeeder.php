<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$y0NcW6snrfAaEbSqDLbxDun10tPmF0K5qC6X2HQeeN0Fw9dQZ/1GW',
                'remember_token' => NULL,
                'created_at' => '2020-10-18 07:09:48',
                'updated_at' => '2020-10-18 07:09:48',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'test',
                'email' => 'test@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$XT.C.//g5CKle4X3n8uWIOOvdw0z9zDBdZMg1CTp.slf6sTHSqNry',
                'remember_token' => NULL,
                'created_at' => '2020-10-18 07:10:13',
                'updated_at' => '2020-10-18 07:10:13',
            ),
        ));
        
        
    }
}