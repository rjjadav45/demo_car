@extends('layouts.app')

@section('content')
        <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info"><b class="text-white">Car Listing</b></div>
                <div class="card-body">
                    <input type="button" name="" class="btn btn-sm btn-success float-right mb-4" value="Add New Car" data-toggle="modal" data-target="#addCarModel">
                    <div class="table-responsive">
                        <table class="table table-bordered yajra-datatable table-sm" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Name</th>
                                <th>Color</th>
                                <th>Make Date</th>
                                <th>Fuel Type</th>
                                <th>Details</th>
                                <th>Images</th>
                                <th>Icon</th>
                                <th>Latitude</th>
                                <th>Longetude</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                 </div>
            </div>
        </div>
        </div>
        @include("cars.add")        
        @include("cars.edit")        
@endsection

@section('customeJS')
<script type="text/javascript">
    
    function preview_images() 
    {
     var total_file=document.getElementById("car_pics").files.length;
     $('#image_preview').empty();
     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<div class='col-md-4 col-4'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"' width='100%' height='auto'></div>");
     }
    }

    function preview_image() 
    {
    $('#icon_preview').empty();

      $('#icon_preview').append("<div class='col-md-4 col-4'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[0])+"' width='100%' height='auto'></div>");
     }

     function old_preview_images() 
    {
     var total_file=document.getElementById("old_car_pics").files.length;
     $('#old_image_preview').empty();
     for(var i=0;i<total_file;i++)
     {
      $('#old_image_preview').append("<div class='col-md-4 col-4'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"' width='100%' height='auto'></div>");
     }
    }

    function old_preview_image() 
    {
    $('#old_icon_preview').empty();
      $('#old_icon_preview').append("<div class='col-md-4 col-4'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[0])+"' width='100%' height='auto'></div>");
     }

 $(function () {
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: false,
        ajax: "{{ route('cars.listing') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'car_name', name: 'name'},
            {data: 'car_color', name: 'Color'},
            {data: 'make_date', name: 'Make Date'},
            {data: 'fuel_type', name: 'Fuel Type'},
            {data: 'car_details', name: 'Details'},
            {data: 'car_pics', name: 'Images'},
            {data: 'car_icon', name: 'Icon'},
            {data: 'latitude', name: 'latitude'},
            {data: 'longetude', name: 'longetude'},
            {data: 'status', name: 'Status'},
            {data: 'created_at', name: 'Created At'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });
  });

 $(document).ready(function(){
    $(document).on("click",".delete_btns",function(){
        var id = $(this).data("id");
        $.ajax({
            type:'GET',
            url: "{{ route('cars.delete') }}",
            data: {"id":id},
            dataType : 'json',
            success : function(data)
            {       
            $('.yajra-datatable').DataTable().ajax.reload();
            }
        });
    });

    $(document).on("click",".edit_btn",function(){
    $("#edit_car_form")[0].reset();
    var id = $(this).data("id");
    $.ajax({
            type:'GET',
            url: "{{ route('getCarDetails') }}",
            data: {"id":id},
            dataType : 'json',
            success : function(data)
            {       
                    $("#old_id").val(data.id);
                    $("#old_car_name").val(data.car_name);
                    $("#old_car_details").val(data.car_details);
                    $("#old_car_details").val(data.car_details);
                    $("#old_status").val(data.status);
                    $("#old_latitude").val(data.latitude);
                    $("#old_longitude").val(data.longetude);
                    var date = new Date(data.make_date);
                    $("#old_date").val(date.getDay());
                    $("#old_month").val(date.getMonth());
                    $("#old_year").val(date.getFullYear());


                    $('.old_ckb_color').each(function () {
                        if(data.car_color.search($(this).val()) > -1){
                            $(this).prop('checked', true);
                        }     
                    });
                    $("#old_car_color").val(data.car_color);
                    $('.old_fule_type').each(function () {
                        if(data.fuel_type.search($(this).val()) > -1){
                            $(this).prop('checked', true);
                        }     
                    });
                    $("#old_dh_fule_type").val(data.fuel_type);
                    var images = JSON.parse(data.car_pics);
                    if(images.length > 0){
                        var imgHTML = "";
                        $.each(images,function(index,value){
                          $('#old_image_preview').empty();
                          imgHTML += "<div class='col-md-4 col-4'><img class='img-responsive' src='{{ asset('images/')}}/"+value+"' width='100%' height='auto'></div>";
                        });
                            $('#old_image_preview').append(imgHTML);
                    }

                        var imgHTML = "";
                        if(data.car_icon){
                            $('#old_icon_preview').empty();
                            imgHTML += "<div class='col-md-4 col-4'><img class='img-responsive' src='{{ asset('images/')}}/"+data.car_icon+"' width='100%' height='auto'></div>";
                            $('#old_icon_preview').append(imgHTML);
                        }
                    
                }
        }); 
    });


    $(".ckb_color").click(function(){
        if($(this).prop("checked") == true){
            $("#car_color-error").remove();
            var color = $(this).val();
            val = $("#car_color").val();
            val = val + "," + color;
            $("#car_color").val(val);
        }else{
            var color = $(this).val();
            val = $("#car_color").val();
            var items = val.split(',');
            val = $.grep(items, function(value) {
              return value != color;
            });
            $("#car_color").val(val);
        }
    });
 
    $(".old_ckb_color").click(function(){
        if($(this).prop("checked") == true){
            $("#car_color-error").remove();
            var color = $(this).val();
            val = $("#old_car_color").val();
             if(val.search(color) < 0)
             {
                 val = val + "," + color;
                $("#old_car_color").val(val);
             }
        }else{
            var color = $(this).val();
            val = $("#old_car_color").val();
            var items = val.split(',');
            val = $.grep(items, function(value) {
              return value != color;
            });
            $("#old_car_color").val(val);
        }
    });
 
    $(".fule_type").click(function(){
            $("#dh_fule_type-error").remove();
            var type = $(this).val();
            $("#dh_fule_type").val(type);
    });
    
    $(".old_fule_type").click(function(){
        $("#old_dh_fule_type-error").remove();
        var type = $(this).val();
        $("#old_dh_fule_type").val(type);
    });
    
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        if(value){
             return regexpr.test(value);
        }else{
            return true;
        }
   }, "Please enter a valid formet Data.");    
    
    $.validator.addMethod("is_image", function(value, element) {         
    var files = document.getElementById("car_pics").files;
    for (var i = files['length']-1;i >= 0; i--) {
        var file = files[i];
        var t = file.type.split('/').pop().toLowerCase();
        if (t != "jpeg" && t != "jpg" && t != "png" && t != "gif") {
            return false;
        }
    }
    return true;
    }, "Please select a valid Image.");    

    $.validator.addMethod("is_image1", function(value, element) {         
        if(document.getElementById("car_icon").value != ""){
            var files = document.getElementById("car_icon").files[0];
            var file = files;
            var t = file.type.split('/').pop().toLowerCase();
            if (t != "jpeg" && t != "jpg" && t != "png" && t != "gif") {
                return false;
            }
        }
    return true;
    }, "Please select a valid Image.");

    $.validator.addMethod("is_image_edit", function(value, element) {         
    var files = document.getElementById("old_car_pics").files;
    for (var i = files['length']-1;i >= 0; i--) {
        var file = files[i];
        var t = file.type.split('/').pop().toLowerCase();
        if (t != "jpeg" && t != "jpg" && t != "png" && t != "gif") {
            return false;
        }
    }
    return true;
    }, "Please select a valid Image.");    

    $.validator.addMethod("is_image1_edit", function(value, element) {         
        if(document.getElementById("old_car_icon").value != ""){
            var files = document.getElementById("old_car_icon").files[0];
            var file = files;
            var t = file.type.split('/').pop().toLowerCase();
            if (t != "jpeg" && t != "jpg" && t != "png" && t != "gif") {
                return false;
            }
        }
    return true;
    }, "Please select a valid Image.");    

    
    $("#edit_car_form").validate({
          ignore: [],
          rules: {
            car_name  : "required",
            car_color : "required",
            car_details : "required",
            dh_fule_type : "required",
            date : "required",
            month : "required",
            year : "required",
            status : "required",
            latitude : {
                        regex : /^[0-9.]+$/
                       },
            longitude : {
                        regex : /^[0-9.]+$/
                       },
            "car_pics[]" : {
                        is_image_edit : true,
                        },
            car_icon : {
                            is_image1_edit : true,
                            }
          },
          messages: {
            car_color : "Please select at list one color.",
          }
    });

    $("#add_car_form").validate({
          ignore: [],
          rules: {
            car_name  : "required",
            car_color : "required",
            car_details : "required",
            dh_fule_type : "required",
            date : "required",
            month : "required",
            year : "required",
            status : "required",
            latitude : {
                        regex : /^[0-9.]+$/
                       },
            longitude : {
                        regex : /^[0-9.]+$/
                       },
            "car_pics[]" : {
                        required : true,
                        is_image : true,
                        },
            car_icon : {
                            is_image1 : true,
                            }
          },
          messages: {
            car_color : "Please select at list one color.",
          }
    });

    $('#edit_car_form').submit(function(e) {
            e.preventDefault();
 
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{ route('cars.update') }}",
                data: formData,
                dataType : 'json',
                cache:false,
                contentType: false,
                processData: false,
                success : function(data)
                {
                    if(data.status == true){
                        $("#edit_car_form")[0].reset();
                        $('#old_image_preview').empty();
                        $('#old_icon_preview').empty();
                        $(".close").click();
                        $('.yajra-datatable').DataTable().ajax.reload();
                    }else{
                        alert("Somthing Want Wrong !");
                    }
                }
            }); 
    });


    $('#add_car_form').submit(function(e) {
            e.preventDefault();
 
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{ route('cars.store') }}",
                data: formData,
                dataType : 'json',
                cache:false,
                contentType: false,
                processData: false,
                success : function(data)
                {
                    if(data.status == true){
                        $("#add_car_form")[0].reset();
                        $('#image_preview').empty();
                        $('#icon_preview').empty();
                        $(".close").click();
                        $('.yajra-datatable').DataTable().ajax.reload();
                    }else{
                        alert("Somthing Want Wrong !");
                    }
                }
            }); 
    });
 });
</script>
@endsection
