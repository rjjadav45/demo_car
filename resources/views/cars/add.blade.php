  <!-- The Add Car Modal -->
  <div class="modal fade" id="addCarModel">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add New Car</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form action="{{ route('cars.store') }}" method="post" id="add_car_form" enctype="multipart/form-data">
          @csrf
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="name">Car Name * : </label>
                <input type="text" class="form-control" id="car_name" placeholder="Enter Car Name" name="car_name">
              </div>
              <div class="form-group col-md-6">
                <label for="name">Car Color * : </label>
              <div class="row m-auto">
                <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input ckb_color"  name="color[]" value="red" > Red
                  </label>
                </div>
                <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input ckb_color" name="color[]" value="green"> Green
                  </label>
                </div>
                <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input ckb_color" name="color[]" value="blue"> Blue
                  </label>
                </div>
                </div>
                <input type="hidden" name="car_color" value="" id="car_color">
                <p class="text-danger er" id='a_er_color'></p>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Car Details * : </label>
                <textarea name="car_details" class="form-control" placeholder="Car Details"></textarea>
                <p class="text-danger er" id='a_er_car_details'></p>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Fuel Type * : </label>
                <div class="row m-auto">
                  <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input fule_type" value="patrol" name="fule_type"> Patrol
                  </label>
                </div>
                <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input fule_type" value="diesel" name="fule_type"> Diesel
                  </label>
                </div>
                <div class="form-check col-3">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input fule_type" value="CNG" name="fule_type"> CNG
                  </label>
                </div>
                <input type="hidden" name="dh_fule_type" value="" id="dh_fule_type">
                </div>
                <p class="text-danger er" id='a_er_fule_type'></p>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Make Date * : </label>
                <div class="row">
                  <div class="col-md-4 col-4">
                    <select class="form-control" name="date">
                      <option value="">Date</option>
                      @for($i=1;$i<=31;$i++)
                      <option value="{{ $i ?? '' }}">{{ $i ?? '' }}</option>
                      @endfor
                    </select>
                  </div>
                  <div class="col-md-4 col-4">
                    <select class="form-control" name="month">
                      <option value="">Month</option>
                      @for($i=1;$i<=12;$i++)
                      <option value="{{ $i ?? '' }}">{{ date('F', mktime(0, 0, 0, $i, 10)) ?? '' }}</option>
                      @endfor
                    </select>
                  </div>
                  <div class="col-md-4 col-4">
                    <select class="form-control" name="year">
                      <option value="">Year</option>
                      @for($i=2021;$i >= 2000;$i--)
                      <option value="{{ $i ?? '' }}">{{ $i ?? '' }}</option>
                      @endfor
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Status * : </label>
                    <select class="form-control" name="status">
                      <option value="">Status</option>
                      <option value="1" selected="">Active</option>
                      <option value="0">InActive</option>
                    </select>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Latitude  : </label>
                <input type="text" class="form-control" id="latitude" placeholder="Enter Latitude" name="latitude">
              </div>
              <div class="form-group col-md-6">
                <label for="name">Longitude  : </label>
                <input type="text" class="form-control" id="longitude" placeholder="Enter Longitude" name="longitude">
              </div>
              <div class="form-group col-md-6">
                <label for="name">Car Pics * : </label>
                <input type="file" class="form-control p-1" id="car_pics"  name="car_pics[]" multiple="" onchange="preview_images();" accept="image/*">
                <div id="image_preview" class="row pt-4">      
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="name">Car Icon  : </label>
                <input type="file" class="form-control p-1" id="car_icon"  name="car_icon"  onchange="preview_image();" accept="image/*">
                <div id="icon_preview" class="row pt-4">
                </div>
              </div>
            </div>
          </div> 
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
